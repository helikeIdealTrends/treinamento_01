@extends('master')
@php
    /** @var \App\User[] $users */
@endphp

@section('content')

    <h1>Users</h1>
    <a href="{{ route('users.add') }}">Add User</a>
    <hr>

    <table data-table-theme="default zebra">
        <thead>
        <tr>
            <th width="35%">User</th>
            <th width="35%">Email</th>
            <th width="30%">&nbsp;</th>
        </tr>
        </thead>

        @foreach($users as $user)
            <tbody>
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>
                    <a href="{{ route('users.edit', ['id' => $user->id]) }}">Edit</a>
                    <a href="{{ route('users.show', ['id' => $user->id]) }}">Show</a>
                    <a href="{{ route('users.delete', ['id' => $user->id]) }}" onclick="if(!confirm('Delete {{$user->id}}?')) return false;">Delete</a>
                </td>
            </tr>
            </tbody>
        @endforeach
    </table>

@endsection
