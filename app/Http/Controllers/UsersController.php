<?php

namespace App\Http\Controllers;

use App\Repositories\UsersRepository;
use App\Service\UserService;
use App\User;
use Illuminate\Http\Request;

/**
 * Class UsersController
 * @package App\Http\Controllers
 */
class UsersController extends Controller
{
    /** @var UsersRepository */
    private $userRepository;
    /** @var UserService */
    private $userService;

    public function __construct(UsersRepository $usersRepository, UserService $userService)
    {
        $this->userRepository = $usersRepository;
        $this->userService = $userService;
    }

    public function index()
    {
        $users = $this->userRepository->model()->orderBy('id')->get();
        return view('users.index', [
            'users' => $users
        ]);
    }

    public function add(Request $request)
    {
        if ($request->post()) {
            $this->userService->save($request);

            return redirect()->action('UsersController@index');
        }

        return view('users.add');
    }

    public function edit(Request $request, $id)
    {
        $user = $this->userRepository->find($id);
        if ($request->post()) {
            $this->userService->save($request, $id);

            return redirect()->action('UsersController@index');
        }

        return view('users.edit', [
            'user' => $user
        ]);
    }

    public function show($id)
    {
        $user = $this->userRepository->find($id);
        return view('users.show', [
            'user' => $user
        ]);
    }

    public function delete($id)
    {
        $this->userRepository->delete($id);
        return redirect()->action('UsersController@index');
    }
}
