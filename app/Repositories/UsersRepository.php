<?php
namespace App\Repositories;

use App\User;

/**
 * Class UsersRepository
 * @package App\Repositories
 */
class UsersRepository extends BaseRepository {
    /** @var User $model */
    public $model;

    /**
     * UsersRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->model = $user;
    }
}