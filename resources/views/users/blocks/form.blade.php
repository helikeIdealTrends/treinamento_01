<label for="name">Name:</label>
{!! Form::text('name', null, ['class' => 'form-control']) !!}<br><br>

<label for="email">Email:</label>
{!! Form::email('email', null, ['class' => 'form-control']) !!}<br><br>

<label for="password">Password:</label>
{!! Form::password('password', ['class' => 'form-control']) !!}<br><br>

<button type="submit">Submit</button>
