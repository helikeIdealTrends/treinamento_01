<?php
namespace App\Service;

use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserService {
    /**
     * @param Request $request
     * @param int $id
     */
    public function save(Request $request, $id = 0)
    {
        /** @var User $user */
        $user = User::firstOrNew(['id' => $id]);
        $user->fill($request->all());
        $user->password = Hash::make($request['password']);
        $user->save();
    }
}
