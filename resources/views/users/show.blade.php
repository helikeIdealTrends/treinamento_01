@extends('master')
@php
    /** @var \App\User $user */
@endphp

@section('content')
    <h1>Show User</h1>
    <hr>
    <table data-table-theme="default zebra">
        <tr>
            <td><b>Name:</b> {{ $user->name }}</td>
        </tr>
        <tr>
            <td><b>Email:</b> {{ $user->email }}</td>
        </tr>
        <tr>
            <td><b>Created at:</b> {{ $user->created_at }}</td>
        </tr>
        <tr>
            <td><b>Updated at:</b> {{ $user->updated_at }}</td>
        </tr>
    </table>
@endsection
