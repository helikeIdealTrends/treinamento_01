@extends('master')

@section('content')
    <h1>Add User</h1>
    <hr>
    {!! Form::open(['method' => 'post']) !!}
        @include('users.blocks.form')
    {!! Form::close() !!}
@endsection
