<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::group(['as' => 'users.'], function () {
    Route::get('/', 'UsersController@index')->name('index');
    Route::any('/add', 'UsersController@add')->name('add');
    Route::any('/edit/{id}', 'UsersController@edit')->name('edit');
    Route::get('/show/{id}', 'UsersController@show')->name('show');
    Route::any('/delete/{id}', 'UsersController@delete')->name('delete');
});
