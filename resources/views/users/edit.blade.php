@extends('master')

@section('content')
    <h1>Edit User</h1>
    <hr>
    {!! Form::model($user, ['method' => 'post']) !!}
        @include('users.blocks.form')
    {!! Form::close() !!}
@endsection
